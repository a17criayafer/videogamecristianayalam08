package cat.inspedralbes.m8.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;

public class GameOverScreen implements Screen {
	// Referencia a joc
	AyaCriVideogame joc;

	// Coses propies de la Screen

	// Imatge del logo
	Texture logo;

	// Variables per a saber si han passat els 3 segons
	float tempsDesdeIniciJoc = 0;
	static int TEMPS_SPLASHSCREEN_S = 3;
	
	//Variable que sirve para poner la puntuación al final
	int puntuacionJuego;
	
	/**
	 * Constructora de la clase
	 * @param joc
	 * @param puntuacion
	 */
	public GameOverScreen(AyaCriVideogame joc, int puntuacion) {
		this.joc = joc;
		logo = new Texture("bg_purple.png");
		puntuacionJuego = puntuacion;
	}
	
	@Override
	public void show() {

	}

	@Override
	public void render(float delta) {
		tempsDesdeIniciJoc += delta;

		// Si ha passat un temps, canvio a la screen del joc.
		if (tempsDesdeIniciJoc > TEMPS_SPLASHSCREEN_S) {
			// Quan ha passat el temps vull canviar
			// Creo objecte i li dono referencia
			joc.setScreen(new JocScreen(joc));
		} else {				
			// dibuixo LOGO
			int ample = Gdx.graphics.getWidth();
			int alt = Gdx.graphics.getHeight();
			
			String textoPuntuacion = "Tu puntuación ha sido de " + this.puntuacionJuego + " puntos.";

			// Dibuixo estirant a tota la pantalla
			joc.batch.begin();
			joc.batch.draw(logo, 0, 0, ample, alt);
			joc.font.draw(joc.batch, textoPuntuacion, ample * .5f, alt * .5f);
			joc.batch.end();
		}
		
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		logo.dispose();	
	}
}
