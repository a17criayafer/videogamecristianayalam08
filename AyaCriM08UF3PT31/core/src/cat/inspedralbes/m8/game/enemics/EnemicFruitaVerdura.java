package cat.inspedralbes.m8.game.enemics;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import cat.inspedralbes.m8.game.Constants;

public class EnemicFruitaVerdura implements IEnemic {
	
	public Rectangle rec;
	Texture texture;
	
	public EnemicFruitaVerdura(float x, float y, float ample, float alt, Texture tex) {
		rec = new Rectangle(x, y, ample, alt);
		texture = tex;
	}

	// Actualitzarse vol dir avancar cap avall
	@Override
	public void actualitzarse(float delta) {
		rec.y -= delta * Constants.VELOCITAT_ENEMIC;
		//rec.y-=20;
	}

	// Dibuixar-se vol dir pintar una textura en forma quadrada (la funcionalitat
	// per defecte de draw)
	// a partir de la coordinada x,y
	@Override
	public void dibuixarse(SpriteBatch batchObert) {
		batchObert.draw(texture, rec.x, rec.y, rec.width, rec.height);
	}

	// Dir si xoca es dir si el rectangle intern de lenemic
	// se solapa amb el player
	@Override
	public boolean solapa(Rectangle player) {
		return this.rec.overlaps(player);
	}

	@Override
	public boolean foraDePantalla() {
		return rec.y < -rec.height;
	}

	@Override
	public String tipusEnemic() {
		return "EnemicFruitaVerdura";
	}
}
