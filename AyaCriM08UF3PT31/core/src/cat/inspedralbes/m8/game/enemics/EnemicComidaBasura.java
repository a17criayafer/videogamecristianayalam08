package cat.inspedralbes.m8.game.enemics;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import cat.inspedralbes.m8.game.Constants;

public class EnemicComidaBasura implements IEnemic {
	
	public Rectangle rec;
	Texture texture;
	boolean capALaDreta = true;

	public EnemicComidaBasura(float x, float y, float ample, float alt, Texture tex) {
		rec = new Rectangle(x, y, ample, alt);
		texture = tex;
	}

	// Actualitzarse vol dir avancar en zig zag
	@Override
	public void actualitzarse(float delta) {
		rec.y -= delta * Constants.VELOCITAT_ENEMIC;

		if (capALaDreta) {
			// moviment
			rec.x += delta * Constants.VELOCITAT_HORIZONTAL_ENEMICZIGZAG;
			// xoc dret
			if ((rec.x + rec.width) > Constants.MON_AMPLE) {
				rec.x = Constants.MON_AMPLE - rec.width;
				capALaDreta = false;
			}
		} else {
			rec.x -= delta * Constants.VELOCITAT_HORIZONTAL_ENEMICZIGZAG;
			// xoc esquerre
			if (rec.x < 0) {
				rec.x = 0;
				capALaDreta = true;
			}
		}
	}

	@Override
	public void dibuixarse(SpriteBatch batchObert) {
		batchObert.draw(texture, rec.x, rec.y, rec.width, rec.height);
	}

	@Override
	public boolean solapa(Rectangle player) {
		return this.rec.overlaps(player);
	}


	@Override
	public boolean foraDePantalla() {
		return rec.y < -rec.height;
	}

	@Override
	public String tipusEnemic() {
		return "EnemicComidaBasura";
	}
}
