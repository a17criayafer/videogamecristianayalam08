package cat.inspedralbes.m8.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Hud implements Disposable {
	public Stage stage;
    private Viewport viewport;
    
    private int score;
    private static Label scoreLabel;
    
    /**
     * Constructora
     * @param sb
     */
    public Hud(SpriteBatch sb) {
    	score = 0;
    	
    	//setup the HUD viewport using a new camera seperate from our gamecam
        //define our stage using that viewport and our games spritebatch
        viewport = new FitViewport(Constants.MON_AMPLE, Constants.MON_ALT, new OrthographicCamera());
        stage = new Stage(viewport, sb);
        
        //define a table used to organize our hud's labels
        Table table = new Table();
        //Top-Align table
        table.top();
        //make the table fill the entire stage
        table.setFillParent(true);
        
        scoreLabel = new Label(String.format("%04d", score), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        table.add(scoreLabel).expandX().padTop(10);
        
        stage.addActor(table);
    }
    
    /**
     * Funcion que actualiza la puntuación
     * @param value
     */
    public void addScore(int value){
        score += value;
        scoreLabel.setText(String.format("%04d", score));
    }
    
    /**
     * Función que retorna la puntuación
     * @return
     */
    public int getScore() {
    	return this.score;
    }

	@Override
	public void dispose() {
		stage.dispose();
	}
}
