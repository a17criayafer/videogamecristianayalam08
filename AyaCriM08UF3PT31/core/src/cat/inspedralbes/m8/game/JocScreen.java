package cat.inspedralbes.m8.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import cat.inspedralbes.m8.game.enemics.EnemicComidaBasura;
import cat.inspedralbes.m8.game.enemics.EnemicFruitaVerdura;
import cat.inspedralbes.m8.game.enemics.IEnemic;


public class JocScreen extends ScreenAdapter {
	// Referencia a joc
	AyaCriVideogame joc;
	
	// Creem una camera que utilitzarem mes endavant
	OrthographicCamera camera;
	Viewport viewport;
	
	//Model de dades
	Rectangle player;
	List<IEnemic> enemics;
	
	float tempsUltimEnemic = 0; //En aquesta variable acumulem el temps passat des de l'ultima aparicio d'enemic 
	
	//Grafics
	Texture playerTexture;	
	Texture fonsTexture;
	
	//Texturas de la comida basura y de la fruta y verdura
	Texture enemyComidaBasuraTexture;
	Texture enemyFrutasVerdurasTexture;
	ArrayList<Texture> enemicsComidaBasuraTextures;
	ArrayList<Texture> enemicsFrutasVerdurasTextures;
	
	//Vector amb els assets per cada tipus enemic
	String[] texturasComidaBasura = {"cocacola.png", "donut.png", "hamburguesa.png", "helado.png", "hotdog.png", "kebab.png", "kfc.png", "patatasFritas.png", "pizza.png", "refresco.png", "salchicha.png", "tacos.png"};
	String[] texturasFrutasVerduras = {"brocoli.png", "calabaza.png", "fresa.png", "kiwi.png", "limon.png", "manzana.png", "pimiento.png", "sandia.png", "zanahoria.png"};
	
	//Hud con la puntuacion en pantalla
	Hud hud;
	
	//Sonido cada vez que se suma un punto
	Sound sound;
	
	/**
	 * Constructora
	 * @param joc
	 */
	public JocScreen(AyaCriVideogame joc) {
		this.joc = joc;
		
		// Configurem amb la resolucio del nostre monS
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Constants.MON_AMPLE, Constants.MON_ALT);
		viewport = new FitViewport(Constants.MON_AMPLE, Constants.MON_ALT, camera);
		
		player = new Rectangle(0, 0, Constants.AMPLE_PLAYER, Constants.ALT_PLAYER);
		
		//Inicialment les llistes buides
		enemics = new ArrayList<IEnemic>();
		
		playerTexture = new Texture("cestaCompra.png");
		
		//Generamos los dos arrays de textures para ir cambiando las imagenes de los enemigos
		
		enemicsComidaBasuraTextures = new ArrayList<>();
		enemicsFrutasVerdurasTextures = new ArrayList<>();
		
		String pathComidaBasura = "ComidaBasura/";
		String pathFrutasVerduras = "FrutasVerduras/";
		
		for (int i = 0; i < texturasComidaBasura.length; i++) {
			String pathFile = pathComidaBasura + this.texturasComidaBasura[i];
			Texture texturaAuxiliar = new Texture(pathFile);
			enemicsComidaBasuraTextures.add(texturaAuxiliar);
		}
		
		for (int i = 0; i < texturasFrutasVerduras.length; i++) {
			String pathFile = pathFrutasVerduras + this.texturasFrutasVerduras[i];
			Texture texturaAuxiliar = new Texture(pathFile);
			enemicsFrutasVerdurasTextures.add(texturaAuxiliar);
		}
		
		hud = new Hud(this.joc.batch);
		fonsTexture = new Texture("new_bg.jpg");
		sound = Gdx.audio.newSound(Gdx.files.internal("sumaPunto.mp3"));
	}
	
	public void resize(int width, int height) {
		viewport.update(width, height);
	}
		
	@Override
	public void render(float delta) {
		//1.Gestió input
		gestioInput();
		
		//2.Càlculs enemics
		actualitza();
		
		//3.Dibuixat
		//Molt important. El dibuixat s'ha de fer entre un begin i un end
		
		//Les dues seguents linies son molt important perque la classe que dibuixa i la camera han de tenir la mateixa idea
		//de la geometria de les coses del joc. Mes endavant veure mes sentit a aixo
		camera.update();
		joc.batch.setProjectionMatrix(camera.combined);
		
		joc.batch.begin();
		
		//El que apareix primer, es dibuixa primer i altres coses el poden tapar

		//Primer fag el BackGround
		joc.batch.draw(fonsTexture, 0, 0, Constants.MON_AMPLE, Constants.MON_ALT);
		
		joc.batch.draw(playerTexture, player.x, player.y, player.width, player.height);

		// Dibuixar. Joc no dibuixa. Joc demana a enemic que es dibuixi
		for (IEnemic enemic : enemics) {
			enemic.dibuixarse(joc.batch);
		}
		
		joc.batch.end();
		
		//Set our batch to now draw what the Hud camera sees.
        this.joc.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
	}
	
	private void actualitza() {
		//1. Si ha passat un cert temps apareix un enemic
		
		/* Elegimos la textura de manera aleatoria */
		Random r = new Random();
	    int numeroRandomImagenes = r.nextInt(((this.enemicsComidaBasuraTextures.size() - 1) - 0) + 1) + 0;
	    enemyComidaBasuraTexture = enemicsComidaBasuraTextures.get(numeroRandomImagenes);
	    
	    numeroRandomImagenes = r.nextInt(((this.enemicsFrutasVerdurasTextures.size() - 1) - 0) + 1) + 0;
	    enemyFrutasVerdurasTexture = enemicsFrutasVerdurasTextures.get(numeroRandomImagenes);

		// Obtenim el temps que ha passat des de l'ultim dibuixat de pantalla. Es el temps d'un frame.
		float delta = Gdx.graphics.getDeltaTime();

		//L'acumulem al temps per controlar si ha d'apareixer un enemic
		tempsUltimEnemic += delta;
		
		if(tempsUltimEnemic > Constants.TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
			int x_ComidaBasura = new Random().nextInt(Constants.MON_AMPLE); //800
			int y = Constants.MON_ALT;
						
			IEnemic nouEnemicComidaBasura = new EnemicComidaBasura(x_ComidaBasura, y, Constants.AMPLE_ENEMIC, Constants.ALT_ENEMIC, enemyComidaBasuraTexture);

			enemics.add(nouEnemicComidaBasura);
			tempsUltimEnemic = 0; //Molt important resetejar el comptador
		}
		
		tempsUltimEnemic += delta;
		
		if(tempsUltimEnemic > Constants.TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
			int x_FrutaVerdura = new Random().nextInt(Constants.MON_AMPLE); //800
			int y = Constants.MON_ALT;
						
			IEnemic nouEnemicFrutaVerdura = new EnemicFruitaVerdura(x_FrutaVerdura, y, Constants.AMPLE_ENEMIC, Constants.ALT_ENEMIC, enemyFrutasVerdurasTexture);

			enemics.add(nouEnemicFrutaVerdura);
			tempsUltimEnemic = 0; //Molt important resetejar el comptador
		}
		
		// Si la puntuacion es mayor que 10, aumentamos la velocidad
		int puntuacio = hud.getScore();
		if (puntuacio >= 10) {
			delta += 0.05;
		}
		
		// Canviem moures
		// Ara el joc no li diu a lenemic com ha d'avancar
		// El joc li diu, han passat delta segons. Mou-te.
		for (IEnemic enemic : enemics) {
			enemic.actualitzarse(delta);
 		}
	
		
		//Mirem si algun enemic esta xocant amb el player
		//Les colisions entre rectangles podrien complicar-se un poc
		//Afortunadament, hi ha un metode que diu si dos rectangles es solapen
		for (Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			IEnemic enemic = (IEnemic) iterator.next();
			
			if(enemic.solapa(player)) {
				String tipusEnemic = enemic.tipusEnemic();
				if (tipusEnemic.equals("EnemicComidaBasura")) {
					joc.setScreen(new GameOverScreen(joc, hud.getScore())); //Aqui va el gameover screen
				}
				else {
					//Sonará un sonido
					sound.play(1.0f);
					hud.addScore(1);				
					iterator.remove();
				}
			}
		}
		
		//Els enemics que surten de la pantalla per baix, son eliminats de la List
		//Eliminar d'una llista mentre s'esta recorrent no es pot fer amb un for normal.
		//Una manera es amb iterators

		for (Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			IEnemic enemic = (IEnemic) iterator.next();
			
			// Detectar quan surt per baix ha de tenir en compte l'alt de la nau
			if (enemic.foraDePantalla()) {
				Gdx.app.debug("elimino", "");
				iterator.remove();
			}
		}
	}

	private void gestioInput() {
		
		//Necessito el temps delta per a calcular les velocitats
		// SABEM QUE
		// velocitat = espai / temps;
		// espai_ara = espai_abans + increment_espai
		// PER TANT
		// espai = velocitat * temps;
		// espai_ara = espai_abans + velocitat*delta;
		
		float delta = Gdx.graphics.getDeltaTime();
		
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			//Ens movem a l'esquerra
			player.x -= Constants.VELOCITAT_JUGADOR * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			//Ens movem a la dreta
			player.x += Constants.VELOCITAT_JUGADOR * delta;
		}
		
		
		//Per controlar que el player no surti de la pantalla s'ha d'evitar que la seva 
		//coordenada x sigui inferior a 0 i que sigui superior a la mida de la pantalla
		//menys el seu ample
		int amplePantalla =  Constants.MON_AMPLE;
		int alturaPantalla = Constants.MON_ALT;
		
		if (player.x < 0) player.x = 0;
		if (player.x > (amplePantalla - player.width)) player.x = amplePantalla - player.width;
	}
	
	//s'executa una vegada al final
		@Override
		public void dispose () {
			playerTexture.dispose();
			enemyComidaBasuraTexture.dispose();
			enemyFrutasVerdurasTexture.dispose();
			fonsTexture.dispose();
			sound.dispose();
		}
}
