package cat.inspedralbes.m8.activitats.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import cat.inspedralbes.m8.activitats.videogameCristianAyala;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.title = "Joc de naus espacials";
		config.width = 800;
		config.height = 480;
		config.fullscreen=false;
		
		new LwjglApplication(new videogameCristianAyala(), config);
	}
}
