package cat.inspedralbes.m8.activitats;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class videogameCristianAyala extends Game {
	//Aqui deixem les coses que volem compartir entre totes les pantalles
	//La classe que dibuixa
	public SpriteBatch batch;	

	//S'executa una sola vegada al principi
	@Override
	public void create () {
		batch = new SpriteBatch();
		
		//Nomes engegar el joc anem a la screen del logo
		//Passem la ref a la screen
		setScreen(new SplashScreen(this));
		
		// Activem el sistema de Log amg el nivell DEBUG
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
	}

	//s'executa continuament quan a libGDX li dona la gana
	@Override
	public void render () {
		//Neteja pantalla
		Gdx.gl.glClearColor(0, 0, 0, 1); //Color de neteja RGBA
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Aquest metode crida al render del pare (classe Game) i aquest crida al render
		// de la screen activa. sense aques super.render no es veura res.
		super.render();
	}
		
	//s'executa una vegada al final
	@Override
	public void dispose () {
		batch.dispose();
	}
}
