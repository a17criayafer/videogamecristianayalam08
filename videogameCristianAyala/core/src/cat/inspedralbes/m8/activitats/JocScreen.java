package cat.inspedralbes.m8.activitats;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import cat.inspedralbes.m8.activitats.enemics.EnemicZigZag;
import cat.inspedralbes.m8.activitats.enemics.IEnemic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


public class JocScreen extends ScreenAdapter {
	// Referencia a joc
	videogameCristianAyala joc;
	
	// Creem una camera que utilitzarem mes endavant

	OrthographicCamera camera;
	Viewport viewport;
	
	//Model de dades
	Rectangle player;
	List<IEnemic> enemics;
	List<Rectangle> disparsJugador;
	
	float tempsUltimEnemic = 0; //En aquesta variable acumulem el temps passat des de l'ultima aparicio d'enemic 
	
	//Grafics
	Texture playerTexture;
	Texture enemyTexture;
	Texture disparTexture;
	Texture fonsTexture;
	
	public JocScreen(videogameCristianAyala joc) {
		this.joc = joc;
		
		// Configurem amb la resolucio del nostre monS
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Constants.MON_AMPLE, Constants.MON_ALT);
		viewport = new FitViewport(Constants.MON_AMPLE, Constants.MON_ALT, camera);
		
		player = new Rectangle(0, 0, Constants.AMPLE_PLAYER, Constants.ALT_PLAYER);
		
		//Inicialment les llistes buides
		enemics = new ArrayList<IEnemic>();
		disparsJugador= new ArrayList<Rectangle>();
		
		playerTexture = new Texture("nausEspacials/player.png");
		enemyTexture = new Texture("nausEspacials/enemy.png");
		disparTexture = new Texture("lasers/laserGreen.png");
		fonsTexture = new Texture("bg_purple.png");
		
	}
	
	public void resize(int width, int height) {
		viewport.update(width, height);
	}
		
	@Override
	public void render(float delta) {
		//1.Gestió input
		gestioInput();
		
		//2.Càlculs enemics
		actualitza();
		
		//3.Dibuixat
		//Molt important. El dibuixat s'ha de fer entre un begin i un end
		
		//Les dues seguents linies son molt important perque la classe que dibuixa i la camera han de tenir la mateixa idea
		//de la geometria de les coses del joc. Mes endavant veure mes sentit a aixo
		camera.update();
		joc.batch.setProjectionMatrix(camera.combined);
		
		joc.batch.begin();
		
		//El que apareix primer, es dibuixa primer i altres coses el poden tapar

		//Primer fag el BackGround
		joc.batch.draw(fonsTexture, 0, 0, Constants.MON_AMPLE, Constants.MON_ALT);
		
		joc.batch.draw(playerTexture, player.x, player.y, player.width, player.height);

		// Dibuixar. Joc no dibuixa. Joc demana a enemic que es dibuixi
		for (IEnemic enemic : enemics) {
			enemic.dibuixarse(joc.batch);
		}
		
		for (Rectangle dispar : disparsJugador) {
			joc.batch.draw(disparTexture, dispar.x, dispar.y, dispar.width, dispar.height);
		}
		
		joc.batch.end();
	}
	
	private void actualitza() {
		//1. Si ha passat un cert temps apareix un enemic

		// Obtenim el temps que ha passat des de l'ultim dibuixat de pantalla. Es el temps d'un frame.
		float delta = Gdx.graphics.getDeltaTime();

		//L'acumulem al temps per controlar si ha d'apareixer un enemic
		tempsUltimEnemic += delta;
		
		if(tempsUltimEnemic > Constants.TEMPS_ENTRE_APARICIO_ENEMIC_EN_SEGONS) {
			int x = new Random().nextInt(Constants.MON_AMPLE); //800
			int y = Constants.MON_ALT;
			
			IEnemic nouEnemic = new EnemicZigZag(x, y, Constants.AMPLE_ENEMIC, Constants.ALT_ENEMIC, enemyTexture);

			enemics.add(nouEnemic);
			tempsUltimEnemic = 0; //Molt important resetejar el comptador
		}
		
		for (Rectangle dispar : disparsJugador) {
			//volem que els dispars vagin cap amunt. Per tant sumem a la y
			dispar.y += Constants.VELOCITAT_DISPAR_PLAYER * delta;
		}
		
		// Canviem moures
		// Ara el joc no li diu a lenemic com ha d'avancar
		// El joc li diu, han passat delta segons. Mou-te.
		for (IEnemic enemic : enemics) {
			enemic.actualitzarse(delta);
 		}
	
		
		//Mirem si algun enemic esta xocant amb el player
		//Les colisions entre rectangles podrien complicar-se un poc
		//Afortunadament, hi ha un metode que diu si dos rectangles es solapen
		for (IEnemic enemic : enemics) {
			if(enemic.solapa(player)) {
				joc.setScreen(new SplashScreen(joc));
			}
		}
		
		// Mirem si algun dispar esta xocant amb algun enemic
		for (Iterator iterEnemics = enemics.iterator(); iterEnemics.hasNext();) {
			IEnemic enemic = (IEnemic) iterEnemics.next();

			for (Iterator iterDispar = disparsJugador.iterator(); iterDispar.hasNext();) {
				Rectangle dispar = (Rectangle) iterDispar.next();

				//Si l'enemic que estem mirant xoca amb el dispar que estem mirant volem eliminar-los als dos
				if(enemic.solapa(dispar)) {
					iterDispar.remove();
					iterEnemics.remove();

					//Si no acabo el bucle de dispar, potser torna a accedir a un dispar ja inexistent
					break;
				}
			}
		}
		
		//Els enemics que surten de la pantalla per baix, son eliminats de la List
		//Eliminar d'una llista mentre s'esta recorrent no es pot fer amb un for normal.
		//Una manera es amb iterators

		for (Iterator iterator = enemics.iterator(); iterator.hasNext();) {
			IEnemic enemic = (IEnemic) iterator.next();
			
			// Detectar quan surt per baix ha de tenir en compte l'alt de la nau
			if (enemic.foraDePantalla()) {
				Gdx.app.debug("elimino", "");
				iterator.remove();
			}
		}
	}

	private void gestioInput() {
		
		//Necessito el temps delta per a calcular les velocitats
		// SABEM QUE
		// velocitat = espai / temps;
		// espai_ara = espai_abans + increment_espai
		// PER TANT
		// espai = velocitat * temps;
		// espai_ara = espai_abans + velocitat*delta;
		
		float delta = Gdx.graphics.getDeltaTime();
		
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			//Ens movem a l'esquerra
			player.x -= Constants.VELOCITAT_NAU_JUGADOR * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			//Ens movem a la dreta
			player.x += Constants.VELOCITAT_NAU_JUGADOR * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			//Ens movem a la alça
			player.y += Constants.VELOCITAT_NAU_JUGADOR * delta;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			//Ens movem a la baixa
			player.y -= Constants.VELOCITAT_NAU_JUGADOR * delta;
		}
		
		// NOU DISPAR OK

		if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			//Fem que el dispar surti de la part central de dalt de la nau
			float x  = player.x + player.width/2;
			float y = player.y + player.height; //Recordem que la y es el punt de baix a lesquerra. 
			disparsJugador.add(new Rectangle(x,y,Constants.AMPLE_LASER,Constants.ALT_LASER));
		}
		
		//Per controlar que el player no surti de la pantalla s'ha d'evitar que la seva 
		//coordenada x sigui inferior a 0 i que sigui superior a la mida de la pantalla
		//menys el seu ample
		int amplePantalla =  Constants.MON_AMPLE;
		int alturaPantalla = Constants.MON_ALT;
		
		if (player.x < 0) player.x = 0;
		if (player.x > (amplePantalla - player.width)) player.x = amplePantalla - player.width;
		if (player.y < 0) player.y = 0;
		if (player.y > (alturaPantalla - player.height)) player.y = alturaPantalla - player.height;
	}
	
	//s'executa una vegada al final
		@Override
		public void dispose () {
			playerTexture.dispose();
			enemyTexture.dispose();
			disparTexture.dispose();
			fonsTexture.dispose();
		}
}
